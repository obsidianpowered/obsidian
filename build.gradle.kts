plugins {
    alias(libs.plugins.gitpatcher)
}

subprojects {
    group = "org.obsidianpowered"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
    }

    apply {
        plugin("java-library")
    }

    tasks.withType<JavaCompile> {
        options.encoding = "UTF-8"
        options.release = 8
    }

    tasks.withType<Javadoc> {
        options.encoding = "UTF-8"
    }

    tasks.withType<Jar> {
        destinationDirectory = file("$rootDir/build")
    }
}

gitPatcher.patchedRepos {
    create("nms") {
        submodule = "work/mc-dev"
        target = file("nms")
        patches = file("patches/nms")
    }

    create("api") {
        submodule = "work/bukkit"
        target = file("obsidian-api")
        patches = file("patches/api")
    }

    create("server") {
        submodule = "work/craftbukkit"
        target = file("obsidian-server")
        patches = file("patches/server")
    }
}