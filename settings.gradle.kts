include(":nms")
include(":obsidian-api")
include(":obsidian-server")

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            plugin("shadow", "com.github.johnrengelman.shadow").version("8.1.1")
        }
    }
}


rootProject.name = "obsidian"

